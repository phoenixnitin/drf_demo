# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
	DEPARTMENT_CHOICES = (
		("HS", "Hospitality"),
		("FY", "Finance"),
	)
	photo = models.ImageField(upload_to="static/", null=True, blank=True)
	department = models.CharField(max_length=2, choices=DEPARTMENT_CHOICES)
