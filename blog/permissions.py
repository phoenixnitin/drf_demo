from rest_framework import permissions
from rest_framework.compat import is_authenticated


class UserPermission(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object to edit it.
    Assumes the model instance has an `owner` attribute.
    """

    def has_permission(self, request, view):
    	# if view.action == "list":
    	# 	return request.user and is_authenticated(request.user)
    	return True

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return request.user and is_authenticated(request.user)

        # Instance must have an attribute named `owner`.
        return obj == request.user