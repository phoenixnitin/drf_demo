from .models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):

    photo = serializers.ImageField(use_url=False, required=False)

    def create(self, validated_data):
        # password = None
        # if "password" in validated_data:
        #   password = validated_data["password"]
        #   validated_data.pop("password")
        user = User.objects.create_user(**validated_data)
        # if password:
        #   user.set_password(password)
        # user.save()
        return user

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        if "password" in validated_data:
            password = validated_data["password"]
            instance.set_password(password)
        instance.photo = validated_data.get('photo', instance.photo)
        instance.department = validated_data.get('department', instance.department)
        instance.save()
        return instance

    class Meta:
        model= User
        fields = ("username", "password", "photo", "department", "id")