# -*- coding: utf-8 -*-
from .serializers import UserSerializer
from rest_framework.viewsets import ModelViewSet
from .models import User
from .permissions import UserPermission

class UserViewSet(ModelViewSet):
	serializer_class = UserSerializer
	queryset = User.objects.all()

	permission_classes = (UserPermission,)
	filter_fields = ('department', )

